﻿using Microsoft.AspNetCore.Mvc;

namespace VersionedApi.Controllers.v1
{
    [Route("api/v{verion:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0", Deprecated = true)]
    public class ValuesController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
    }
}
